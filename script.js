/* when a user clicks, toggle the 'is-animating' class */
$(".heart").on('click touchstart', function(){
  $(this).toggleClass('is_animating');
});

/*when the animation is over, remove the class*/
$(".heart").on('animationend', function(){
  $(this).toggleClass('is_animating');
});

$(".like").ready(function(){
    $("img").click(function(){
        $(this).attr("src", "https://img.icons8.com/ios-filled/32/000000/like.png");
    });    
});

